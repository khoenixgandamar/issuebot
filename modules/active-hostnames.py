#!/usr/bin/env python3
#
#
# issuebot_apt_install = python3-cffi python3-pip python3-requests-cache
# issuebot_pip_install = pycares

import inspect
import os
import sys
import zipfile

localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '..')
)
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)
from issuebot import IssuebotModule

# load vendored ipgrep
from importlib.util import spec_from_file_location, module_from_spec

spec = spec_from_file_location(
    'module.name', os.path.join(localmodule, '.vendor', 'ipgrep.py')
)
ipgrep = module_from_spec(spec)
spec.loader.exec_module(ipgrep)
ASN = ipgrep.ASN
Extractor = ipgrep.Extractor
Host = ipgrep.Host
IPLookup = ipgrep.IPLookup
Resolver = ipgrep.Resolver


class IPGrep(IssuebotModule):
    def main(self):
        # TODO support scanning the source code
        if not self.apkfiles_required():
            return
        self.apk_path = self.apkfiles[0]  # TODO actually go through all APKs
        if not os.access(self.apk_path, os.R_OK):
            return
        resolver = Resolver(timeout=120)
        ip_lookup = IPLookup()
        names, ips = set(), set()

        try:
            with zipfile.ZipFile(self.apk_path) as z:
                for f in z.namelist():
                    for line in z.open(f).readlines():
                        extractor = Extractor(line)
                        names = names | set(extractor.extract_names())
                        ips = ips | set(extractor.extract_ips())
        except zipfile.BadZipFile as e:
            print(e, file=sys.stderr)
            sys.exit(1)

        resolved = resolver.resolve(names)
        hosts_fromnames = set(
            [Host(ip=i, name=n, query_type=q) for i, n, q in resolved]
        )
        hosts_fromips = set([Host(ip=ip) for ip in ips])
        hosts = hosts_fromnames | hosts_fromips

        for host in hosts:
            subnet = ip_lookup.lookup(host.ip)
            asn = ASN(0, "-", "-")
            if subnet:
                asn = ASN(
                    subnet['as_number'],
                    subnet['as_country_code'],
                    "AS{}: {} ({})".format(
                        subnet['as_number'],
                        subnet['as_description'],
                        subnet['as_country_code'],
                    ),
                )
            if not host.name:
                host.name = "-"
            host.asn = asn

        hostnames = []
        ipaddresses = []
        for host in sorted(hosts, key=lambda x: (x.name, x.ip, x.asn.description)):
            if host.name == host.ip:
                ipaddresses.append([host.name, host.ip, host.asn.description])
            else:
                hostnames.append([host.name, host.ip, host.asn.description])
        report = '<h3>Active Hostnames and IP Addresses</h3>'
        report += '<details><summary>hostnames</summary><table>'
        report += '<tr><th>name</th><th>IP</th><th>ASN Description</th></tr>'
        for name, ip, description in hostnames:
            report += '<tr><td><b>{}</b></td><td><tt>{}</tt></td><td>{}<td></tr>\n'.format(
                name, ip, description
            )
        report += '</table></details>'
        report += '<details><summary>IP addresses</summary><table>'
        report += '<tr><th>name</th><th>IP</th><th>ASN Description</th></tr>'
        for name, ip, description in ipaddresses:
            report += '<tr><td><b>{}</b></td><td><tt>{}</tt></td><td>{}<td></tr>\n'.format(
                name, ip, description
            )
        report += '</table></details>'

        if hostnames or ipaddresses:
            self.reply['report'] = report
            self.reply['reportData'] = {
                'hostnames': hostnames,
                'ipAddresses': ipaddresses,
            }
        self.write_json()


if __name__ == '__main__':
    IPGrep().main()
